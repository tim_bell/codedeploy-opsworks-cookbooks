require 'aws-sdk'

s3 = AWS::S3.new
obj = s3.buckets['test-site-config'].objects['fruits.json']
file_content = obj.read

file '/tmp/myobjectfoo.txt' do
  content file_content
  action :create
end

exists = File.file?('/tmp/myobjectfoo.txt')

log "File exists #{exists}" do
    level :info
end